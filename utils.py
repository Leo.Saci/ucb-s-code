import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

q = stats.norm.ppf(0.975)


def plot_regret(time, regret, H, label):
    """Plot the cumulative regret"""

    regret_avg = np.mean(regret, axis=0)
    regret_std = np.std(regret, axis=0)
    nb_it = regret.shape[0]

    plt.figure(0)
    plt.plot(time, regret_avg, label=label)
    plt.fill_between(
        time,
        regret_avg - (q / np.sqrt(nb_it)) * regret_std,
        regret_avg + (q / np.sqrt(nb_it)) * regret_std,
        color="#A9D9D9",
    )

    plt.xlabel("h")
    plt.ylabel("cumulative regret")
    T = len(time) / H
    plt.xticks(T * np.arange(H), np.arange(H) + 1)
    plt.legend()


def plot_setting(parameters):
    """Plot the MABs settings"""
    M, K = parameters.shape
    plt.figure(1)

    for m in range(M):
        labs = ["b_" + str(i + 1) for i in range(M)]
        plt.scatter([i + 1 for i in range(K)], parameters[m], marker="o")
        z = np.polyfit([i + 1 for i in range(K)], parameters[m], K - 1)
        p = np.poly1d(z)
        plt.plot(
            np.linspace(1, K, 1000),
            p(np.linspace(1, K, 1000)),
            "--",
            label=labs[m],
        )
    for i in range(K):
        plt.plot([i + 1, i + 1], [-0.5, 0.5], "--k", linewidth=0.4)

    plt.ylim([-0.5, 0.5])
    plt.xlabel("$\\mathcal{A}$")
    plt.xticks([i + 1 for i in range(K)])
    plt.yticks(list(np.linspace(-0.5, 0.5, 11)))
    plt.legend()
