import numpy as np


class Policy(object):
    def __init__(self):
        pass

    def __str__(self):
        pass

    def choose(self, agent):
        pass


class Random(Policy):
    def __init__(self):
        pass

    def __str__(self):
        return "Random"

    def choose(self, agent):
        if agent.t < K:
            a_t = int(t)
        else:
            a_t = int(np.random.choice([i for i in range(K)]))
        return a_t


class KL_UCB(Policy):
    def __init__(self):
        pass
    
    def __str__(self):
        return "KL-UCB++"
    
    def choose(self, agent):
        t = agent.t
        T = agent.T
        K = agent.K
        h = agent.h

        if t < K:
            a_t = int(t % K)
        else:
            emp_means = agent.emp_mean[h]
            deviations = agent.deviation[h]
            a_t = int(np.argmax(emp_means + deviations))
        return a_t


class UCBS(Policy):
    def __init__(self):
        pass

    def __str__(self):
        return "UCB-S"
    
    def choose(self, agent):
        """
        Choose an action and return it 
        (type int)
        """
        h = agent.h
        t = agent.t
        T = agent.T
        K = agent.K

        if t < K:
            a_t = int(t % K)
        elif h == 0:
            a_t = np.argmax(agent.emp_mean[h] + agent.deviation[h])
        else:

            idx = list(range(K))
            np.random.shuffle(idx)
            
            # Random arm index among the most pulled ones 
            i_max = idx[np.argmax(agent.N[h, idx])]

            # test
            positives = self.test(agent, h, i_max)
            nb_pos = len(positives)

            # agregated data
            agg_N = np.sum(agent.N[positives], axis=0) + agent.N[h]
            agg_sums = np.sum(agent.sums[positives], axis=0) + agent.sums[h]
            agg_means = agg_sums / agg_N
            y = ((nb_pos + 1) * T) / (K * agg_N)
            f_h_t = agent.log_plus(y * (agent.log_plus(y)**2 + 1))
            agg_deviation = np.sqrt((2 * f_h_t) / agg_N)
            a_t = int(np.argmax(agg_means + agg_deviation))
        return a_t

    def test(self, agent, h, i_max):
        """
        Return the list of periods k <= h-1 positively tested
        (type list)
        """
        Z = 100 * np.abs(agent.most_pulled[:h] - i_max) * np.ones((agent.K, h))
        Z = Z.T
        Z = (
            Z
            + np.abs(agent.emp_mean[:h] - agent.emp_mean[h])
            - agent.deviation[:h]
            - agent.deviation[h]
        )
        Z = np.max(Z, axis=1)

        positives = set(np.where(Z <= 0)[0])
        false_positives = positives - agent.adjacence_dict[h]
        true_positives = positives - set(false_positives)
        agent.FP[h, agent.t] = len(false_positives)
        return list(positives)
