import numpy as np
from scipy.stats import bernoulli


class GaussianBandit(object):
    def __init__(self, mu, sigma=1):
        self.mu = mu
        self.gaps = np.max(mu) - mu
        self.sigma = sigma
        self.K = mu.shape[0]
        self.cov = sigma * np.ones(self.K)

    def pull(self, i):
        return np.random.normal(loc=self.mu[i], scale=self.sigma)

    def sample(self, N):
        return np.random.normal(loc=self.mu, scale=self.cov, size=(N, self.K))


class BernoulliBandit(object):
    def __init__(self, mu):
        self.mu = mu
        self.gaps = np.max(mu) - mu
        self.K = mu.shape[0]
    
    def pull(self, i):
        return bernoulli.rvs(self.mu[i], size=1)[0]
    
    def sample(self, N):
        return self.mu * np.ones((N, self.K)) + bernoulli.rvs(p=(0.5, 0.5), size=(N, self.K)) * 2 - 1  