import numpy as np
from bandit import GaussianBandit, BernoulliBandit
from data import build_dataset
from agent import SwitchingAgent
from policy import UCBS, KL_UCB
import matplotlib.pyplot as plt
from utils import plot_regret, plot_setting
from tqdm import tqdm

if __name__ == "__main__":
    np.random.seed(0)
    # Parameters
    nb_it = 30
    K = 2
    M = 2
    T = 10000
    H = 10
    
    # policies
    policies = [KL_UCB(), UCBS()][:2]

    print("Number of models : M = " + str(M))
    print("Number of arms : K = " + str(K))
    print("Number of periods : H = " + str(H))
    print("Time horizon : T = " + str(T))
    print("Number of runs : " + str(nb_it))
    print()

    # Plot the setting and the regret curves (booleans)
    plot_set = True
    plot_reg = True

    # MABs setting
    D = np.sqrt(np.log(H*T)/T)
    parameters = np.array([[-5*D, 5*D], [5*D, -5*D]])
    # parameters = np.random.rand(M, K) - 0.5
    
    if plot_setting:
        plot_setting(parameters)
    bandits = [GaussianBandit(mu) for mu in parameters]

    # bandits switching sequences in [1, H]
    switch_sequences = np.random.randint(0, M, (nb_it, H))
        
    # python list containing a dataset for each run
    datasets = [build_dataset(bandits, T, H, seq) for seq in switch_sequences]


    for pi in policies:
        agent = SwitchingAgent(bandits, pi, T)
        regret = np.zeros((nb_it, H * T))
        print("Policy name : " + pi.__str__())
        for it in tqdm(range(nb_it)):
            data = datasets[it]
            switch_seq = switch_sequences[it]
            agent.run(data, switch_seq)
            regret[it] = agent.regret

        plot_regret(np.arange(H * T) + 1, regret, H, label=pi.__str__())
        print()

    if plot_reg or plot_setting:
        plt.show()
